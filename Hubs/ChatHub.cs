﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalRChat.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        public async Task SendMessage(string userFrom, string message, string userTo)
        {
            await Clients.Groups(userTo).SendAsync("ReceiveMessage", userFrom, message);
        }

        public override Task OnConnectedAsync()
        {
            var userName = Context.GetHttpContext().User.Identity.Name;
            Groups.AddToGroupAsync(Context.ConnectionId, userName);

            return base.OnConnectedAsync();
        }
    }
}