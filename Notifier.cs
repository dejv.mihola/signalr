﻿using Microsoft.AspNetCore.SignalR;
using SignalRChat.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SignalR
{
    public static class Notifier
    {
        private static CancellationTokenSource _cancellationToken;
        private static Thread _thread;
        private static int _runing = 0;

        public static void NotifyStart(int count, IHubContext<ChatHub> hubContext)
        {
            if (_runing == 0)
            {
                _runing = count;
                _cancellationToken = new CancellationTokenSource();
                _thread = new Thread(new ThreadStart(() => RunThread(_cancellationToken.Token, hubContext)));
                _thread.Start();
            }
        }

        public static void NotifyStop()
        {
            _runing = 0;
            _cancellationToken.Cancel();
        }

        private static void RunThread(CancellationToken cancellationToken, IHubContext<ChatHub> hubContext)
        {
            while(_runing-- > 0)
            {
                cancellationToken.WaitHandle.WaitOne(TimeSpan.FromSeconds(5));
                hubContext.Clients.All.SendAsync("ReceiveMessage", "Server", "Notification");
            }
            _runing = 0;
        }
    }
}
