﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR.Models
{
    public class User
    {
        public string UserName { get; set; } = "";
        public string ConnectionId { get; set; } = "";
    }
}
